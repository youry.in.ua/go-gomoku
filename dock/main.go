// Copyright 2015 Google Inc. All rights reserved.
// Use of this source code is governed by the Apache 2.0
// license that can be found in the LICENSE file.

// Sample helloworld is a basic App Engine flexible app.
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	//"google.golang.org/appengine"
)

func main() {

	http.HandleFunc("/", handleRoot)
	http.Handle("/public/", http.FileServer(http.Dir(".")))

	//http.HandleFunc("/gomoku/", handleGomoku)
	//http.Handle("/gomoku/public/", http.FileServer(http.Dir(".")))

	http.HandleFunc("/api", handleGomokuAPI)

	//appengine.Main()
	http.ListenAndServe(":7080", nil)
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	index, err := loadFile("public", "index.html")
	if err == nil {
		fmt.Fprint(w, string(index))
	} else {
		http.NotFound(w, r)
	}
}

/*
func handleGomoku(w http.ResponseWriter, r *http.Request) {
	index, err := loadFile("gomoku/public", "index.html")
	if err == nil {
		fmt.Fprint(w, string(index))
	} else {
		http.NotFound(w, r)
	}
}
*/

func handleGomokuAPI(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err == nil {
		fmt.Fprint(w, calcStep(body))
	}
}
