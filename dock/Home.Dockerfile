FROM aarch64/ubuntu:16.04
WORKDIR /app
#RUN mkdir /app/export
#COPY hosts /etc/
#COPY cert/ptks.crt cert/
#COPY cert/ptks.pem cert/
COPY public/ public/
# copy binary into image
COPY dock-arm64 dock-arm64
ENTRYPOINT ["./dock-arm64"]
