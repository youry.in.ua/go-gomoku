package main

import (
	"io/ioutil"
	"path/filepath"
)

func loadFile(path string, filename string) (b []byte, err error) {
	fullPath := filepath.Join(path, filename)
	b, err = ioutil.ReadFile(fullPath)
	return
}
